import { Component, OnInit } from '@angular/core';
import { DatabaseService, ReturnVal } from '../database.service';

@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.css']
})

export class DatabaseComponent implements OnInit {

  returnval: ReturnVal[];
  var1: string;
  var2: string;
  var3: string;
  var4: string;
  var5: string;
  var6: string;
  var7: string;
  var8: string;
  var9: string;
  var10: string;
  val: string = "";

  constructor(public databaseService: DatabaseService) {
  }
  ngOnInit() {

  }
  submit() {
    this.getQuery(this.val);
  }

  getQuery(val: string) {
    this.databaseService.getQuery(val).subscribe(

      data => {
        console.log(data);
        this.returnval = data;
        console.log(this.returnval);
        this.var1 = "";
        this.var2 = "";
        this.var3 = "";
        this.var4 = "";
        this.var5 = "";
        this.var6 = "";
        this.var7 = "";
        this.var8 = "";
        this.var9 = "";
        this.var10 = "";

        var i = 0;
        for (var x in data[0]) {
          i++;
          console.log(i);

          var field = "var" + i.toString();
          switch (field) {
            case ("var1"):
              this.var1 = x;
              continue;
            case ("var2"):
              this.var2 = x;
              continue;
            case ("var3"):
              this.var3 = x;
              continue;
            case ("var4"):
              this.var4 = x;
              continue;
            case ("var5"):
              this.var5 = x;
              continue;
            case ("var6"):
              this.var6 = x;
              continue;
            case ("var7"):
              this.var7 = x;
              continue;
            case ("var8"):
              this.var8 = x;
              continue;
            case ("var9"):
              this.var9 = x;
              continue;
            case ("var10"):
              this.var10 = x;
              continue;
            default:
              break;
          }
        }
        i = 0;
        console.log(this.var6);
      },
      error => {
        alert('Not a valid query');
      }
    );
  }

}
