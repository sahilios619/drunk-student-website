import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface Drinker{
  phone:string;
  name:string;
  address:string;
  state:string;
  zip:string;
}
export interface LikedBeers{
  beer_name:string;
}
export interface FrequentedBars{
  bar_name:string;
  bar_phone:string;
}
export interface DrinkerTransactions{
  drinker_phone:string;
  drinker_name:string;
  bar_phone:string;
  bar_name:string;
  date:string;
  time:string;
  item_purchased:string;
  item_price:string;
}
export interface TopBarsSpent{
  bar_name:string;
  bar_phone:string;
  total:string;
}
export interface TopBeersOrdered{
  item_name:string;
  count:string;
}

@Injectable({
  providedIn: 'root'
})
export class DrinkersService {

  constructor(public http: HttpClient) { }

  getDrinkers()
  {
    return this.http.get<Drinker[]>('api/drinkers');
  }
  getDrinker(drinker:string)
  {
    return this.http.get<Drinker>('api/drinkers/' + drinker);
  }

  getDrinkerLikes(drinker:string)
  {
    return this.http.get<LikedBeers[]>('api/likes/drinker/' + drinker);
  }
  getDrinkerFrequents(drinker:string)
  {
    return this.http.get<FrequentedBars[]>('api/frequents/drinker/' + drinker);
  }
  getDrinkerTransactions(drinker:string)
  {
    return this.http.get<DrinkerTransactions[]>('api/transactions/drinker/' + drinker);
  }
   getTopBarsSpent(drinker:string)
  {
    return this.http.get<TopBarsSpent[]>('api/topbarsspent/drinker/' + drinker);
  }
  getTopBeersOrdered(drinker:string)
  {
    return this.http.get<TopBeersOrdered[]>('api/topbeersordered/drinker/' + drinker);
  } 
}
