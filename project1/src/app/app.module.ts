import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';

import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DatabaseComponent } from './database/database.component';

import { BarDetailsComponent } from './bar-details/bar-details.component';
import { BarsComponent } from './bars/bars.component';

import { BeerDetailsComponent } from './beer-details/beer-details.component';
import { BeersComponent } from './beers/beers.component';

import { DrinkerDetailsComponent } from './drinker-details/drinker-details.component';
import { DrinkersComponent } from './drinkers/drinkers.component';

import { PopupComponent} from './popup/popup.component';



@NgModule({
  declarations: [
    AppComponent,
    DatabaseComponent,
    BarDetailsComponent,
    BarsComponent,
    BeerDetailsComponent,
    BeersComponent,
    DrinkerDetailsComponent,
    DrinkersComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DropdownModule,
    FormsModule,
    BrowserAnimationsModule,
    TableModule,
    AppRoutingModule,
    ModalModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
