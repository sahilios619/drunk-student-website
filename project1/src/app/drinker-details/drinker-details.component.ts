import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DrinkersService, Drinker, FrequentedBars,LikedBeers, DrinkerTransactions,TopBarsSpent,TopBeersOrdered} from '../drinkers.service';
import { HttpResponse } from '@angular/common/http';
declare const Highcharts: any;

@Component({
  selector: 'app-drinker-details',
  templateUrl: './drinker-details.component.html',
  styleUrls: ['./drinker-details.component.css']
})
export class DrinkerDetailsComponent implements OnInit {

  drinkerName:string;
  drinkerDetails:Drinker;
  frequentedBars:FrequentedBars[];
  likedBeers:LikedBeers[];
  drinkerTransactions:DrinkerTransactions[];
  topBeersOrdered:TopBeersOrdered[];
  topBarsSpent:TopBarsSpent[];

  constructor(
    public drinkerService: DrinkersService,
    public route: ActivatedRoute) 
    {
      route.paramMap.subscribe((paramMap) =>{
        this.drinkerName = paramMap.get('drinker');

        drinkerService.getDrinker(this.drinkerName).subscribe(
          data => {
            this.drinkerDetails = data;
          },
          (error:HttpResponse<any>) =>{
            if(error.status === 404){
              alert('Drinker Not Found');
            }else{
              console.error(error.status + ' - '  + error.body);
            }

          }
        );

        drinkerService.getDrinkerFrequents(this.drinkerName).subscribe(
          data => {
            this.frequentedBars = data;
          }
        );

        drinkerService.getDrinkerLikes(this.drinkerName).subscribe(
          data => {
            this.likedBeers = data;
          }
        );

        drinkerService.getDrinkerTransactions(this.drinkerName).subscribe(
          data => {
            this.drinkerTransactions = data;
          }
        );

        drinkerService.getTopBeersOrdered(this.drinkerName).subscribe(
          data => {
            console.log(data);
            const drinkers = [];
            const counts = [];
            data.forEach(drinker => {
              drinkers.push(drinker.item_name);
              counts.push(drinker.count);
            }
            );
            this.renderTopBeersOrderedChart(drinkers,counts);
          }
        );

        drinkerService.getTopBarsSpent(this.drinkerName).subscribe(
          data => {
            console.log(data);
            const drinkers = [];
            const total = [];
            data.forEach(drinker => {
              drinkers.push(drinker.bar_name);
              total.push(drinker.total);
            }
            );
            this.renderTopBarsSpentChart(drinkers,total);
          }
        );
      });


    }

  ngOnInit() {
  }
  renderTopBarsSpentChart(bars: string[], total: string[]){
    Highcharts.chart('topBarsSpent', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Bars'
      },
      xAxis: {
        categories: bars,
        title: {
          text: 'Bar Name'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Amount Spent' 
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bars: {
          dataLabels:
          {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: total
      }]
    });
  }
  renderTopBeersOrderedChart(beers: string[], counts: string[]){
    Highcharts.chart('topBeersOrdered', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Beers Ordered'
      },
      xAxis: {
        categories: beers,
        title: {
          text: 'Drinker Name'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number Purchased' 
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bars: {
          dataLabels:
          {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: counts
      }]
    });
  }
}
