import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface Beer {
  name: string;
  manufacturer: string;
  calories: string;
  alcohol_percent: string;
  price: string;
}

export interface BeerLocations{
  bar_name: string;
  bar_phone: string;
  price: string;
}

export interface BeerLikers{
  drinker_name:string;
  drinker_phone:string
}
export interface TopBeerBuyers{
  drinker_name:string;
  drinker_phone:string
  num_purchased:string;
}
export interface TopBeerSellers{
  bar_name:string;
  bar_phone:string;
  num_sold:string;
}


@Injectable({
  providedIn: 'root'
})
export class BeersService {

  constructor(public http:   HttpClient) { }


  getBeers(){
    return this.http.get<Beer[]>('api/beers');
  }

  getBeer(beer: string) {
    return this.http.get<Beer>('api/beers/' + beer);
  }

  getBeerLocations(beer: string) {
    return this.http.get<BeerLocations[]>('api/locations/beer/' + beer);
  }


  getBeerLikers(beer: string)
  {
    return this.http.get<BeerLikers[]>('api/likers/beer/' + beer);
  }
  getTopBeerBuyers(beer: string)
  {
    return this.http.get<TopBeerBuyers[]>('api/topbuyers/beer/' + beer);
  }

  getTopBeerSellers(beer: string)
  {
    return this.http.get<TopBeerSellers[]>('api/topsellers/beer/' + beer);
  }

//  getBeerManufacturers(beer ?: string) {
//    if(beer)
//    {
//      return this.http.get<Beer[]>('api/beer-manufacturer/${beer}');
//    }
//    return this.http.get<Beer[]>('api/beer-manufacturer');
//
//  }

}
