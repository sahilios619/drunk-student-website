import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BarsService, Bar} from '../bars.service';
declare const Highcharts: any;

@Component({
  selector: 'app-bars',
  templateUrl: './bars.component.html',
  styleUrls: ['./bars.component.css']
})
export class BarsComponent implements OnInit {

  bars: Bar[];

  constructor(public barservice: BarsService) {
        this.getBar();
 }

  ngOnInit() {
  }

  getBar(){
    this.barservice.getBars().subscribe(
      data=>{
        this.bars = data;
      },
      error=>{
        alert('No bars to display');
      }
    );
  }
}