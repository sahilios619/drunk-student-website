import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BeersService, Beer, BeerLocations, BeerLikers, TopBeerBuyers, TopBeerSellers } from '../beers.service';
import { HttpResponse } from '@angular/common/http';
declare const Highcharts: any;

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.css']
})
export class BeerDetailsComponent implements OnInit {

  beerName: string;
  beerDetails: Beer;
  beerLocations: BeerLocations[];
  beerLikers: BeerLikers[];
  topBeerBuyers: TopBeerBuyers[];
  topBeerSellers: TopBeerSellers[];

  constructor(
    public beerService: BeersService,
    public route: ActivatedRoute) {
    route.paramMap.subscribe((paramMap) => {
      this.beerName = paramMap.get('beer');

    });



    beerService.getBeer(this.beerName).subscribe(
      data => {
        this.beerDetails = data;
      },
      (error: HttpResponse<any>) => {
        if (error.status === 404) {
          alert('Beer Not Found');
        } else {
          console.error(error.status + ' - ' + error.body);
        }

      }
    );

    beerService.getBeerLocations(this.beerName).subscribe(
      data => {
        this.beerLocations = data;
      }
    );

    beerService.getBeerLikers(this.beerName).subscribe(
      data => {
        this.beerLikers = data;
      }
    );


    beerService.getTopBeerBuyers(this.beerName).subscribe(
      data => {
        console.log(data);
        const drinkers = [];
        const counts = [];
        data.forEach(drinker => {
          drinkers.push(drinker.drinker_name);
          counts.push(drinker.num_purchased);
        }
        );
        this.renderTopBeerBuyersChart(drinkers,counts);
      }
    );

    beerService.getTopBeerSellers(this.beerName).subscribe(
      data => {
        console.log(data);
        const bars = [];
        const counts = [];
        data.forEach(bar => {
          bars.push(bar.bar_name);
          counts.push(bar.num_sold);
        }
        );
        this.renderTopBeerSellersChart(bars,counts);

      });
  }

  ngOnInit() {
  }
renderTopBeerSellersChart(bars: string[], counts: string[]){
  Highcharts.chart('topBeerSellers', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Top Bars'
    },
    xAxis: {
      categories: bars,
      title: {
        text: 'Bar Name'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number Sold' 
      },
      labels: {
        overflow: 'justify'
      }
    },
    plotOptions: {
      bars: {
        dataLabels:
        {
          enabled: true
        }
      }
    },
    legend: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    series: [{
      data: counts
    }]
  });
}
renderTopBeerBuyersChart(drinkers: string[], counts: string[]){
  Highcharts.chart('topBeerBuyers', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Top Drinkers'
    },
    xAxis: {
      categories: drinkers,
      title: {
        text: 'Drinker Name'
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number Purchased' 
      },
      labels: {
        overflow: 'justify'
      }
    },
    plotOptions: {
      bars: {
        dataLabels:
        {
          enabled: true
        }
      }
    },
    legend: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    series: [{
      data: counts
    }]
  });
}
}