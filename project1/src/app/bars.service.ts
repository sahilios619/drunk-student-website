import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface Bar{
  phone:string;
  name:string;
  address:string;
  state:string;
  zip:string;
  site:string;
}

export interface BeerMenuItem{
  item_name:string
  price:string
  likes:string
}
export interface FoodMenuItem{
  item_name:string
  price:string
  likes:string
}
export interface DrinkerItem{
  drinker_name:string
  drinker_phone:string
}
export interface TopBeers{
  item_name:string
  num_beers:string
}
export interface TopDrinkers{
  drinker_name:string
  drinker_phone:string
  total_spent:string
}
export interface TopManufacturers{
  manufacturer:string
  num_beers:string
}


@Injectable({
  providedIn: 'root'
})
export class BarsService {

  constructor(private http: HttpClient) {}

  getBars()
  {
    return this.http.get<Bar[]>('api/bars');
  }
  getBar(bar:string)
  {
    return this.http.get<Bar>('api/bars/' + bar);
  }

  getBeerMenu(bar:string)
  {
    return this.http.get<BeerMenuItem[]>('api/menu/beer/' + bar);

  }
  getFoodMenu(bar:string)
  {
    return this.http.get<FoodMenuItem[]>('api/menu/food/' + bar);

  }



  getFrequenters(bar:string)
  {
    return this.http.get<DrinkerItem[]>('api/frequenters/bar/' + bar);
  }
  getTopBeers(bar: string)
  {
    return this.http.get<TopBeers[]>('api/topbeers/bar/' + bar);
  }
  getTopDrinkers(bar: string)
  {
    return this.http.get<TopDrinkers[]>('api/topdrinkers/bar/' + bar);
  }  
  getTopManufacturers(bar: string)
  {
    return this.http.get<TopManufacturers[]>('api/topmanufacturers/bar/' + bar);
  }
}
