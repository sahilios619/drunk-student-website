import { Component, OnInit } from '@angular/core';

import { BeersService, Beer } from '../beers.service';
//import { SelectItem } from 'primeng/components/common/selectitem';
//declare const Highcharts: any;

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.css']
})
export class BeersComponent implements OnInit {

  beers: Beer[];
//  originalBeerList: Beer[]
//  manufacturerOptions: SelectItem[];

  constructor(public beerservice: BeersService) {
    this.beerservice.getBeers().subscribe(
      data => {
        this.beers = data;
      }
    );


//    this.beerservice.getBeerManufacturers().subscribe(
//      data => {
//        this.manufacturerOptions = data.map(manf => {
//          return {
//            label: manf,
//            data: manf
//          }
//        }
//        );
//      },
//    );
  }
  ngOnInit() {
  }
}




