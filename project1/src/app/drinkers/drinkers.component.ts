import { Component, OnInit } from '@angular/core';
import { DrinkersService, Drinker} from '../drinkers.service';
declare const Highcharts: any;

@Component({
  selector: 'app-drinkers',
  templateUrl: './drinkers.component.html',
  styleUrls: ['./drinkers.component.css']
})
export class DrinkersComponent implements OnInit {

  drinkers: Drinker[];

  constructor(public drinkerService: DrinkersService) {
        this.getDrinker();
      }

  ngOnInit() {
  }

  getDrinker(){
    this.drinkerService.getDrinkers().subscribe(
      data=>{
        this.drinkers = data;
      },
      error=>{
        alert('No drinkers to display');
      }
    );
  }
  
}
