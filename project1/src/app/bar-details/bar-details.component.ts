import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BarsService, Bar, BeerMenuItem, FoodMenuItem,DrinkerItem,TopDrinkers,TopBeers,TopManufacturers} from '../bars.service';
import { HttpResponse } from '@angular/common/http';
declare const Highcharts: any;

@Component({
  selector: 'app-bar-details',
  templateUrl: './bar-details.component.html',
  styleUrls: ['./bar-details.component.css']
})
export class BarDetailsComponent implements OnInit {

  barName:string;
  barDetails:Bar;
  beerMenu: BeerMenuItem[];
  foodMenu: FoodMenuItem[];
  frequentDrinkers: DrinkerItem[];
  topDrinkers: TopDrinkers[]
  topBeers: TopBeers[]
  topManufacturers: TopManufacturers[]

  constructor(
    public barService: BarsService,
    public route: ActivatedRoute) 
    {
      route.paramMap.subscribe((paramMap) =>{
        this.barName = paramMap.get('bar');

        barService.getBar(this.barName).subscribe(
          data => {
            this.barDetails = data;
          },
          (error:HttpResponse<any>) =>{
            if(error.status === 404){
              alert('Bar Not Found');
            }else{
              console.error(error.status + ' - '  + error.body);
            }

          }
        );

        barService.getBeerMenu(this.barName).subscribe(
          data => {
            this.beerMenu = data;
          }
        );
        
        barService.getFoodMenu(this.barName).subscribe(
          data => {
            this.foodMenu = data;
          }
        );

        barService.getFrequenters(this.barName).subscribe(
          data => {
            this.frequentDrinkers = data;
          }
        );
        barService.getTopBeers(this.barName).subscribe(
          data => {
            console.log(data);
            const beers = [];
            const counts = [];
            data.forEach(beer => {
              beers.push(beer.item_name);
              counts.push(beer.num_beers);
            }
            );
            this.renderTopBeersChart(beers,counts);
          }
        );
        barService.getTopDrinkers(this.barName).subscribe(
          data => {
            console.log(data);
            const beers = [];
            const counts = [];
            data.forEach(drinker => {
              beers.push(drinker.drinker_name);
              counts.push(drinker.total_spent);
            }
            );
            this.renderTopDrinkersChart(beers,counts);
          }
        );
        barService.getTopManufacturers(this.barName).subscribe(
          data => {
            console.log(data);
            const manufacturers = [];
            const counts = [];
            data.forEach(manufacturer => {
              manufacturers.push(manufacturer.manufacturer);
              counts.push(manufacturer.num_beers);
            }
            );
            this.renderTopManufacturersChart(manufacturers,counts);
          }
        );
      });
    }

  ngOnInit() {
  }
  renderTopBeersChart(beers: string[], counts: string[]){
    Highcharts.chart('topBeers', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Beers'
      },
      xAxis: {
        categories: beers,
        title: {
          text: 'Beer Name'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number Purchased' 
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bars: {
          dataLabels:
          {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: counts
      }]
    });
  }
  renderTopDrinkersChart(drinkers: string[], total: string[]){
    Highcharts.chart('topDrinkers', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Drinkers'
      },
      xAxis: {
        categories: drinkers,
        title: {
          text: 'Drinker Name'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total Spent' 
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bars: {
          dataLabels:
          {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: total
      }]
    });
  }

  renderTopManufacturersChart(manufacturers: string[], count: string[]){
    Highcharts.chart('topManufacturers', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top Manufacturers'
      },
      xAxis: {
        categories: manufacturers,
        title: {
          text: 'Manufacturer Name'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number Sold' 
        },
        labels: {
          overflow: 'justify'
        }
      },
      plotOptions: {
        bars: {
          dataLabels:
          {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        data: count
      }]
    });
  }
}
