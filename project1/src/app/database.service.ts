import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


export interface ReturnVal{
  
}
@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(public http: HttpClient) { }



  getQuery(query: string)
  {

    return this.http.get<ReturnVal[]>('api/database/' + query);


  }
}
