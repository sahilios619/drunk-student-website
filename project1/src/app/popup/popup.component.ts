import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    templateUrl: './popup.component.html'
})
export class PopupComponent{

    @ViewChild('autoShownModal') public autoShownModal: ModalDirective;
    public isModalShown: boolean = false;

    public showModal(): void {       
        this.isModalShown = true;        
    }

    public hideModal(): void {
        this.autoShownModal.hide();
    }

    public onHidden(): void {
        this.isModalShown = false;
    }
}