Bar Beer Drinker project utilizes Python, MySQL,AngularJS, HTML
Data was generated using python

Website Components
	Bars Page
		Displays all the bars in the database
	Beers Page
		Displays all the beers in the database
	Drinkers Page
		Displays all the drinkers in the database
	Database Page
		SQL query box runs queries and displays the results in a table below
		Can do insert, update and deletion queries as well

Data Restrictions
	1. A drinker can only frequent a bar in their own state
	2. A transaction cannot occur when the bar is closed
	3. If Beer 1 is less than Beer 2 in bar 1 , then Beer 1 has to be less than Beer 2 in bar 2 
