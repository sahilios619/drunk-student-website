from flask import Flask
from flask import jsonify
from flask import make_response
from flask import request
import json

from Project1 import database


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World'

@app.route('/api/drinkers' , methods = ["GET"])
def get_drinkers():
    return jsonify(database.get_drinkers())

@app.route('/api/drinkers/<name>' , methods = ["GET"])
def find_drinker(name):
    try:
        if name is None:
            raise ValueError("Drinker is not specified")
        drinker = database.find_drinker(name)
        if drinker is None:
            return make_response("No drinker found", 404)
        return jsonify(drinker)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)



@app.route('/api/bars' , methods = ["GET"])
def bars():
    return jsonify(database.get_bars())

@app.route('/api/bars/<name>' , methods = ["GET"])
def find_bar(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified")
        bar = database.find_bar(name)
        if bar is None:
            return make_response("No bar found", 404)
        return jsonify(bar)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)



@app.route('/api/beers' , methods = ["GET"])
def get_beers():
    return jsonify(database.get_beers())
    
@app.route('/api/beers/<name>' , methods = ["GET"])
def find_beer(name):
    try:
        if name is None:
            raise ValueError("Beer is not specified")
        beer = database.find_beer(name)
        if beer is None:
            return make_response("No beer found", 404)
        return jsonify(beer)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)








# FOR A GIVEN BAR...
@app.route('/api/menu/beer/<name>' , methods = ["GET"])
def get_beer_menu(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        bar = database.find_bar(name)
        if bar is None:
            raise ValueError("No bar found with given name")
        return jsonify(database.get_beer_menu(name))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/menu/food/<name>' , methods = ["GET"])
def get_food_menu(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        bar = database.find_bar(name)
        if bar is None:
            raise ValueError("No bar found with given name")
        return jsonify(database.get_food_menu(name))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/frequenters/bar/<name>' , methods = ["GET"])
def get_bar_frequenters(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified")
        frequenters = database.get_bar_frequenters(name)
        return jsonify(frequenters)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topbeers/bar/<name>' , methods = ["GET"])
def get_top_beers(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified")
        top_beers = database.get_top_beers(name)
        return jsonify(top_beers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topdrinkers/bar/<name>' , methods = ["GET"])
def get_top_drinkers(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified")
        top_drinkers = database.get_top_drinkers(name)
        return jsonify(top_drinkers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topmanufacturers/bar/<name>' , methods = ["GET"])
def get_top_manufacturers(name):
    try:
        if name is None:
            raise ValueError("Bar is not specified")
        top_manufacturers = database.get_top_manufacturers(name)
        return jsonify(top_manufacturers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)





# FOR A GIVEN BEER...
@app.route('/api/locations/beer/<name>' , methods = ["GET"])
def get_beer_locations(name):
    try:
        if name is None:
            raise ValueError("Beer is not specified")
        beer = database.find_beer(name)
        if beer is None:
            raise ValueError("No beer found with given name")
        return jsonify(database.get_beer_locations(name))
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/likers/beer/<name>' , methods = ["GET"])
def get_beer_likers(name):
    try:
        if name is None:
            raise ValueError("Beer is not specified")
        likers = database.get_beer_likers(name)
        return jsonify(likers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topbuyers/beer/<name>' , methods = ["GET"])
def get_top_beer_buyers(name):
    try:
        if name is None:
            raise ValueError("Beer is not specified")
        top_beer_buyers = database.get_top_beer_buyers(name)
        return jsonify(top_beer_buyers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topsellers/beer/<name>' , methods = ["GET"])
def get_top_beer_sellers(name):
    try:
        if name is None:
            raise ValueError("Beer is not specified")
        top_beer_sellers = database.get_top_beer_sellers(name)
        return jsonify(top_beer_sellers)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)






# FOR A GIVEN DRINKER...
@app.route('/api/likes/drinker/<name>' , methods = ["GET"])
def get_drinker_likes(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        likes = database.get_drinker_likes(name)
        return jsonify(likes)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)
        
@app.route('/api/frequents/drinker/<name>' , methods = ["GET"])
def get_drinker_frequents(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        frequents = database.get_drinker_frequents(name)
        return jsonify(frequents)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/transactions/drinker/<name>' , methods = ["GET"])
def get_drinker_transactions(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        transactions = database.get_drinker_transactions(name)
        return jsonify(transactions)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topbeersordered/drinker/<name>' , methods = ["GET"])
def get_top_beers_ordered(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        topbeersordered = database.get_top_beers_ordered(name)
        return jsonify(topbeersordered)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500)

@app.route('/api/topbarsspent/drinker/<name>' , methods = ["GET"])
def get_top_bars_spent(name):
    try:
        if name is None:
            raise ValueError("Name is not specified")
        topbarsspent = database.get_top_bars_spent(name)
        return jsonify(topbarsspent)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500) 





@app.route('/api/database/<query>' , methods = ["GET"])
def get_query(query):
    try:
        if query is None:
            raise ValueError("Query is not specified")
        q = database.get_query(query)
        return jsonify(q)
    except ValueError as e:
        return make_response(str(e), 400)
    except Exception as e:
        return make_response(str(e), 500) 