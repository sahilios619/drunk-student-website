from sqlalchemy import create_engine
from sqlalchemy import sql

from Project1 import config

engine = create_engine(config.database_uri)


"""  
Queries needed to run:


Show data for given beer
    -time distribution of when this beer sells the most
    bar:
=     top drinkers who bought it(how many they bought)
=      which bars sell the most(how many they sold)
    table:
 =       show bars who sell the beer
 =       show drinkers who like the beer



Show data for given bar
    -time distribution of sales
    bar:
 =       top drinkers who have gone there(how much they spent)
 =       top beers they sell (most popular beers...number of beers ordered)
    table:
=        show drinkers who frequent there(frequent customers)
=        show which beers and foods they sell(menu)



Show data for given drinker
    bar:
        top beers they ordered
        how much they spent by day, week, month
    table:
=        show which bars they frequent(favorite bars)
=        show which beers they like (favorite beers)
=        show all their transactions(ordered by time and grouped by bars)



Show data for given transaction

"""



def get_bars():
    with engine.connect() as con:
        rs = con.execute("SELECT name,address,zip,state,phone,site FROM bars;")
        return [dict(row) for row in rs]

def find_bar(phone):
    with engine.connect() as con:
        query = sql.text("SELECT name,address,zip,state,phone,site FROM bars WHERE phone = :phone;")

        rs = con.execute(query, phone = phone)
        result = rs.first()
        if result is None:
            return None
        return dict(result)




def get_beers():
    with engine.connect() as con:
        rs = con.execute("SELECT name,manufacturer,wholesale_price,calories,alcohol_content FROM beers;")
        return [dict(row) for row in rs]
def find_beer(name):
    with engine.connect() as con:
        query = sql.text("SELECT name,manufacturer,wholesale_price,calories,alcohol_content FROM beers WHERE name = :name;")

        rs = con.execute(query, name = name)
        result = rs.first()
        if result is None:
            return None
        return dict(result)




def get_drinkers():
    with engine.connect() as con:
        rs = con.execute("SELECT name,address,zip,state,phone FROM drinkers;")
        return [dict(row) for row in rs]

def find_drinker(phone):
    with engine.connect() as con:
        query = sql.text("SELECT name,address,zip,state,phone FROM drinkers WHERE phone = :phone;")

        rs = con.execute(query, phone = phone)
        result = rs.first()
        if result is None:
            return None
        return dict(result)









# FOR A GIVEN BAR
def get_bar_hours(bar_phone):
    with engine.connect() as con:
        query = sql.text("SELECT * FROM hours Where bar_phone = :bar_phone;")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]

def get_beer_menu(bar_phone):
    with engine.connect() as con:
        query = sql.text("SELECT item_name,price FROM sells Where type = 'beer' AND bar_phone = :bar_phone;")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]

def get_food_menu(bar_phone):
    with engine.connect() as con:
        query = sql.text("SELECT item_name,price FROM sells WHERE type = 'food' AND bar_phone =:bar_phone;")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]

def get_bar_frequenters(bar_phone):
    with engine.connect() as con:
        query = sql.text("SELECT drinker_name, drinker_phone FROM frequents WHERE bar_phone = :bar_phone;")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]    
        
def get_top_drinkers(bar_phone):
    with engine.connect() as con:        
        query = sql.text("SELECT drinker_name,drinker_phone, SUM(item_price) AS 'total_spent' FROM transactions WHERE bar_phone = :bar_phone GROUP BY drinker_name ORDER BY total_spent DESC LIMIT 5")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]

def get_top_beers(bar_phone):
    with engine.connect() as con:         
        query = sql.text("Select t.item_name, count(t.item_name ) as 'num_beers' from transactions t where t.bar_phone = :bar_phone and t.item_name in(select item_name from sells where type = 'beer')group by t.item_name ORDER BY num_beers DESC LIMIT 5")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]

def get_top_manufacturers(bar_phone):
    with engine.connect() as con:         
        query = sql.text("Select b.manufacturer, count(t.item_name ) as 'num_beers' from transactions t, beers b where t.bar_phone = :bar_phone and t.item_name = b.name and t.item_name in(select item_name from sells where type = 'beer')group by b.manufacturer ORDER BY num_beers DESC LIMIT 5")
        rs = con.execute(query, bar_phone = bar_phone)
        return [dict(row) for row in rs]



# FOR A GIVEN BEER
def get_beer_likers(beer):
    with engine.connect() as con:
        query = sql.text("SELECT drinker_name,drinker_phone FROM likes WHERE beer_name = :beer;")
        rs = con.execute(query, beer = beer)
        return [dict(row) for row in rs]

def get_beer_locations(beer_name):
    with engine.connect() as con:
        query = sql.text("SELECT bar_name,bar_phone,price FROM sells Where item_name = :beer_name ORDER BY price ASC;")
        rs = con.execute(query, beer_name = beer_name)
        return [dict(row) for row in rs]

def get_top_beer_buyers(beer_name):
    with engine.connect() as con:        
        query = sql.text("SELECT drinker_name,drinker_phone, COUNT(drinker_name) AS 'num_purchased' FROM transactions WHERE item_name = :beer_name GROUP BY drinker_name ORDER BY num_purchased DESC LIMIT 5")
        rs = con.execute(query, beer_name = beer_name)
        return [dict(row) for row in rs]

def get_top_beer_sellers(beer_name):
    with engine.connect() as con:         
        query = sql.text("SELECT bar_name,bar_phone, COUNT(bar_name) AS 'num_sold' FROM transactions WHERE item_name = :beer_name GROUP BY bar_name ORDER BY num_sold DESC LIMIT 5")
        rs = con.execute(query, beer_name = beer_name)
        return [dict(row) for row in rs]

        
        




 # FOR A GIVEN DRINKER
def get_drinker_likes(drinker_phone):
    with engine.connect() as con:
        query = sql.text("SELECT beer_name FROM likes WHERE drinker_phone = :drinker_phone;")
        rs = con.execute(query, drinker_phone = drinker_phone)
        return [dict(row) for row in rs]

def get_drinker_frequents(drinker_phone):
    with engine.connect() as con:
        query = sql.text("SELECT bar_name,bar_phone FROM frequents WHERE drinker_phone = :drinker_phone;")
        rs = con.execute(query, drinker_phone = drinker_phone)
        return [dict(row) for row in rs]

def get_drinker_transactions(drinker_phone):
    with engine.connect() as con:
        query = sql.text("SELECT * FROM transactions WHERE drinker_phone = :drinker_phone;")
        rs = con.execute(query, drinker_phone = drinker_phone)
        return [dict(row) for row in rs]
 
def get_top_beers_ordered(drinker_phone):
    with engine.connect() as con:        
        query = sql.text("SELECT item_name, COUNT(item_name) AS 'count' FROM transactions WHERE drinker_phone = :drinker_phone and item_name in(select item_name from sells where type = 'beer') GROUP BY item_name ORDER BY count DESC LIMIT 5")
        rs = con.execute(query, drinker_phone = drinker_phone)
        return [dict(row) for row in rs]

def get_top_bars_spent(drinker_phone):
    with engine.connect() as con:         
        query = sql.text("SELECT bar_name,bar_phone, SUM(item_price) AS 'total' FROM transactions WHERE drinker_phone = :drinker_phone GROUP BY bar_name ORDER BY total DESC LIMIT 5")
        rs = con.execute(query, drinker_phone = drinker_phone)
        return [dict(row) for row in rs]
 





def get_query(query):
    with engine.connect() as con:         
        rs = con.execute(query)
        return [dict(row) for row in rs]